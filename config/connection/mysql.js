const mysql = require("mysql");
const { MYSQL_SERVER, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } =
process.env;
const koneksi = mysql.createConnection({
    host: MYSQL_SERVER,
    port: MYSQL_PORT,
    user: MYSQL_USER,
    password: MYSQL_PASSWORD,
    database: MYSQL_DATABASE,
});

koneksi.connect(function(err) {
    if (err) throw err;
    console.log("MySQL Connected...");
});

module.exports = koneksi;