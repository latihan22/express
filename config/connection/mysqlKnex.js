const { MYSQL_SERVER, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } =
process.env;
const koneksiKnex = require("knex")({
    client: "mysql",
    connection: {
        host: MYSQL_SERVER,
        port: MYSQL_PORT,
        user: MYSQL_USER,
        password: MYSQL_PASSWORD,
        database: MYSQL_DATABASE,
    },
});

module.exports = koneksiKnex;