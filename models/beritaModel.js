const koneksiKnex = require("../config/connection/mysqlKnex");
const { hendelData, hendelPesan } = require("../utils/hendelResponse");

const beritaModel = {
    insert(res, data) {
        koneksiKnex
            .insert(data)
            .into("berita")
            .then((hasil) => {
                return hendelData(res, 201, hasil[0]);
            })
            .catch((err) => {
                return hendelPesan(res, 500, "Ada kesalahana knex", {
                    error: err,
                });
            });
    },
    get(res, query) {
        let limit = 10;
        const querySql = koneksiKnex.select().from("berita");
        if (query.cari) {
            querySql
                .whereLike("judul", "%" + query.cari + "%")
                .orWhereLike("isi", "%" + query.cari + "%");
        }
        limit = parseInt(query.limit) || limit;
        querySql
            .limit(limit)
            .then((hasil) => {
                return hendelData(res, 201, hasil);
            })
            .catch((err) => {
                return hendelPesan(res, 500, "Ada kesalahana knex", {
                    error: err,
                });
            });
    },
    getById(res, id) {
        koneksiKnex
            .select()
            .from("berita")
            .where("id", id)
            .then((hasil) => {
                return hendelData(res, 201, hasil[0]);
            })
            .catch((err) => {
                return hendelPesan(res, 500, "Ada kesalahana knex", {
                    error: err,
                });
            });
    },
    updateById(res, id, data) {
        koneksiKnex
            .select("id")
            .from("berita")
            .where("id", id)
            .then((hasil) => {
                if (!hasil.length) {
                    return hendelPesan(res, 404, "Tidak Ada Data");
                }
                koneksiKnex("berita")
                    .where("id", id)
                    .update(data)
                    .then((hasil) => {
                        hendelPesan(res, 201, "Berhasil Update Data");
                    })
                    .catch((err) => {
                        return hendelPesan(res, 500, "Ada kesalahana knex", {
                            error: err,
                        });
                    });
            })
            .catch((err) => {
                return hendelPesan(res, 500, "Ada kesalahana knex", {
                    error: err,
                });
            });
    },
    deleteById(res, id) {
        koneksiKnex
            .select("id")
            .from("berita")
            .where("id", id)
            .then((hasil) => {
                if (!hasil.length) {
                    return hendelPesan(res, 404, "Tidak Ada Data");
                }
                koneksiKnex("berita")
                    .where("id", id)
                    .del()
                    .then((hasil) => {
                        hendelPesan(res, 201, "Berhasil Hapus Data");
                    })
                    .catch((err) => {
                        return hendelPesan(res, 500, "Ada kesalahana knex", {
                            error: err,
                        });
                    });
            })
            .catch((err) => {
                return hendelPesan(res, 500, "Ada kesalahana knex", {
                    error: err,
                });
            });
    },
    getAllReturn(req, res, next) {
        koneksiKnex
            .select()
            .from("berita")
            .then((row) => {
                req.dataReturn = row;
                next();
            })
            .catch((err) => {
                req.dataReturn = false;
                next();
            });
    },
};
module.exports = beritaModel;