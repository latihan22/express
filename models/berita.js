const koneksiKnex = require("../config/connection/mysqlKnex");
const bookshelf = require("bookshelf")(koneksiKnex);
const User = require("./user");
const Berita = bookshelf.model("Berita", {
    tableName: "berita",
    user() {
        return this.belongsTo(User, "user", "id");
    },
    comment() {
        return this.hasMany("Cometar", "user", "id");
    },
});

module.exports = Berita;