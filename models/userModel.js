const koneksiKnex = require("../config/connection/mysqlKnex");
const { hendelData, hendelPesan } = require("../utils/hendelResponse");
const userModel = {
    insert(res, data) {
        koneksiKnex
            .select("email")
            .from("user")
            .where("email", data.email)
            .then((hasil) => {
                if (hasil.length) {
                    throw "Data telah Ada";
                }
                koneksiKnex
                    .insert(data)
                    .into("user")
                    .then((hasil) => {
                        return hendelData(res, 201, hasil[0]);
                    })
                    .catch((err) => {
                        return hendelPesan(res, 500, "Ada kesalahana knex", {
                            error: err,
                        });
                    });
            })
            .catch((err) => {
                return hendelPesan(res, 500, "Ada kesalahana", {
                    error: err,
                });
            });
    },
    get(res) {
        koneksiKnex
            .select()
            .from("user")
            .then((hasil) => {
                return hendelData(res, 201, hasil);
            })
            .catch((err) => {
                return hendelPesan(res, 500, "Ada kesalahana", {
                    error: err,
                });
            });
    },

    updateToken(id, data) {
        koneksiKnex("user")
            .update(data)
            .where("id", id)
            .then((hasil) => {
                return console.log("Berhasil Update tokens");
            })
            .catch((err) => {
                return console.log("err", err);
            });
    },
};

module.exports = userModel;