const koneksiKnex = require("../config/connection/mysqlKnex");
const bookshelf = require("bookshelf")(koneksiKnex);
const Berita = require("./berita");

const User = bookshelf.model("User", {
    tableName: "user",
    beritas() {
        return this.hasMany(Berita, "user", "id");
    },
});

module.exports = User;