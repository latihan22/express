const beritaModel = require("../models/beritaModel");

const beritaController = {
    insert(req, res, next) {
        const data = {
            judul: req.body.judul,
            isi: req.body.isi,
        };
        beritaModel.insert(res, data);
    },
    get(req, res, next) {
        beritaModel.get(res, req.query);
    },
    getById(req, res, next) {
        beritaModel.getById(res, req.params.id);
    },
    updateById(req, res, next) {
        const data = {
            judul: req.body.judul,
            isi: req.body.isi,
        };
        beritaModel.updateById(res, req.params.id, data);
    },
    deleteById(req, res, next) {
        beritaModel.deleteById(res, req.params.id);
    },
};

module.exports = beritaController;