const userModel = require("../models/userModel");
const bcrypt = require("bcryptjs");
const { validationResult } = require("express-validator");

const userController = {
    get(req, res, next) {
        userModel.get(res);
    },
    async insert(req, res, next) {
        try {
            const encryptedPassword = await bcrypt.hash(req.body.password, 10);
            const data = {
                name: req.body.name,
                email: req.body.email,
                password: encryptedPassword,
            };
            userModel.insert(res, data);
        } catch (error) {}
    },
};
module.exports = userController;