const { hendelPesan } = require("../utils/hendelResponse");
const userModel = require("../models/userModel");
const koneksi = require("../config/connection/mysql");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const authConroller = {
    async login(req, res, next) {
        const { email, password } = req.body;
        if (!(email && password)) {
            return hendelPesan(
                res,
                402,
                "Email Dan passwor tidak boleh kosong"
            );
        }
        koneksi.query(
            "SELECT * FROM user WHERE email = ?",
            email,
            async(err, rows, field) => {
                if (err) {
                    return hendelPesan(res, 500, "Terjadi Kesalahan", {
                        error: err,
                    });
                }
                if (rows.length < 1) {
                    return hendelPesan(res, 404, "Tidak Ada Data");
                }

                const user = rows[0];
                if (user && (await bcrypt.compare(password, user.password))) {
                    let token = jwt.sign({
                            id: user.id,
                            email,
                        },
                        process.env.TOKEN_KEY, {
                            expiresIn: "1h",
                        }
                    );
                    if (user.token) {
                        try {
                            jwt.verify(user.token, process.env.TOKEN_KEY);
                        } catch (error) {
                            user.token = token;
                            userModel.updateToken(user.id, { token: token });
                        }
                    } else {
                        user.token = token;
                        userModel.updateToken(user.id, { token: token });
                    }
                    return res
                        .status(201)
                        .json({
                            token: user.token,
                        })
                        .end();
                }
                return hendelPesan(res, 503, "Invalid Credentials");
            }
        );
    },
};
module.exports = authConroller;