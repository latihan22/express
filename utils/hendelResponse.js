const hendelData = (response, statusCode, values) => {
    let data = {
        data: values,
    };
    response.status(statusCode).json(data);
    response.end();
};

const hendelPesan = (response, statusCode, message, error = {}) => {
    let data = {
        message: message,
        ...error,
    };
    response.status(statusCode).json(data);
    response.end();
};

module.exports = { hendelData, hendelPesan };