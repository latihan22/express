module.exports = {
    up: `CREATE TABLE berita (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        judul varchar(255) DEFAULT NULL,
        isi text,
        user bigint(20) DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE KEY id (id)
      ) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1`,
    down: "DROP TABLE IF EXISTS berita",
};