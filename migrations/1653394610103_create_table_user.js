module.exports = {
    up: ` CREATE TABLE user(
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        name varchar(255) DEFAULT NULL,
        email varchar(255) DEFAULT NULL,
        password varchar(255) DEFAULT NULL,
        token varchar(255) DEFAULT NULL,
        PRIMARY KEY(id),
        UNIQUE KEY id(id)
    ) ENGINE = InnoDB AUTO_INCREMENT = 27 DEFAULT CHARSET = latin1;
    `,
    down: `DROP TABLE IF EXISTS user`,
};