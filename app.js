require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const app = express();
const { PORT_SERVER } = process.env;
const port = PORT_SERVER;

// set body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(
    morgan("tiny", {
        skip: function(req, res) {
            return res.statusCode < 400;
        },
    })
);
// route
app.use("/", require("./routes"));
app.use("/api/berita", require("./routes/berita"));
app.use("/api/user", require("./routes/user"));
app.use("/auth", require("./routes/auth"));

app.listen(port, () => {
    console.log(`Ini Berjalan Di  http://localhost:${port}`);
});