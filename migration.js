require("dotenv").config();
const mysql = require("mysql");
const migration = require("mysql-migrations");
const { MYSQL_SERVER, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } =
process.env;

var connection = mysql.createPool({
    connectionLimit: 10,
    host: MYSQL_SERVER,
    port: MYSQL_PORT,
    user: MYSQL_USER,
    password: MYSQL_PASSWORD,
    database: MYSQL_DATABASE,
});

migration.init(connection, __dirname + "/migrations", () => {
    console.log("Selesai Create Migration");
});