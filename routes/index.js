const express = require("express");
const router = express.Router();
const beritaModel = require("../models/beritaModel");
const Users = require("../models/user");
const koneksi = require("../config/connection/mysql");
router.get("/", (req, res) => {
    new Users()
        .fetchPage({
            pageSize: 10,
            page: 1,
            withRelated: ["beritas"],
        })
        .then((data) => {
            console.log(data.pagination, "pagina");
            res.status(200).json({ data, ...data.pagination });
        })
        .catch((err) => {
            return console.log("err", err);
        });
});
async function getBerita() {
    return new Promise((resolf, rejek) => {
        koneksi.query("SELECT * from berita", (err, row) => {
            if (err) {
                return rejek(err.sqlMessage);
            }
            resolf(row);
        });
    });
}
router.get("/tes", async(req, res) => {
    try {
        const data = await getBerita();
        res.status(200).json(data);
    } catch (error) {
        res.status(500).json({ message: error });
    }
});
router.get("/blog", beritaModel.getAllReturn, (req, res, next) => {
    res.status(200).json(req.dataReturn);
});

module.exports = router;