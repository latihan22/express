const express = require("express");
const router = express.Router();

var users = [
    { id: 0, name: "tj", email: "tj@vision-media.ca", role: "member" },
    { id: 1, name: "ciaran", email: "ciaranj@gmail.com", role: "member" },
    {
        id: 2,
        name: "aaron",
        email: "aaron.heckmann+github@gmail.com",
        role: "admin",
    },
];

function loadUser(req, res, next) {
    // You would fetch your user from the db
    var user = users[req.params.id];
    if (user) {
        req.user = user;
        next();
    } else {
        next(new Error("Failed to load user " + req.params.id));
    }
}

function andRestrictToSelf(req, res, next) {
    // If our authenticated user is the user we are viewing
    // then everything is fine :)
    if (req.authenticatedUser.id === req.user.id) {
        next();
    } else {
        // You may want to implement specific exceptions
        // such as UnauthorizedError or similar so that you
        // can handle these can be special-cased in an error handler
        // (view ./examples/pages for this)
        next(new Error("Unauthorized"));
    }
}

function andRestrictTo(role) {
    return function(req, res, next) {
        if (req.authenticatedUser.role === role) {
            next();
        } else {
            next(new Error("Unauthorized"));
        }
    };
}

router.use(function(req, res, next) {
    req.authenticatedUser = users[2];
    next();
});

router.get("/", function(req, res) {
    res.redirect("user/0");
    // res.send("ApiUser");
});

router.get("/:id", loadUser, function(req, res) {
    res.send("Viewing user " + req.user.name);
});

router.get("/:id/edit", loadUser, andRestrictToSelf, function(req, res) {
    res.send("Editing user " + req.user.name);
});

router.delete("/:id", loadUser, andRestrictTo("admin"), function(req, res) {
    res.send("Deleted user " + req.user.name);
});

module.exports = router;