const express = require("express");
const beritaController = require("../controllers/beritaController");
const router = express.Router();
const validator = require("../middleware/validator");
const { check } = require("express-validator");
const auth = require("../middleware/auth");
router.get("/", beritaController.get);
router.get("/:id", beritaController.getById);
router.use(auth);
router.post(
    "/",
    check("judul").notEmpty().withMessage("wajib di isi"),
    check("isi").notEmpty().withMessage("wajib di isi"),
    validator,
    beritaController.insert
);
router
    .route("/:id")
    .put(
        check("judul").notEmpty().withMessage("wajib di isi"),
        check("isi").notEmpty().withMessage("wajib di isi"),
        validator,
        beritaController.updateById
    )
    .delete(beritaController.deleteById);

module.exports = router;