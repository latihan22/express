const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../middleware/auth");
const validator = require("../middleware/validator");
const { check } = require("express-validator");
router.use(auth);
router.get("/", userController.get);
router.post(
    "/", [
        check("name").notEmpty().withMessage("wajib di isi"),
        check("email")
        .notEmpty()
        .withMessage("wajib di isi")
        .isEmail()
        .withMessage("format email"),
        check("password")
        .notEmpty()
        .withMessage("wajib di isi")
        .isLength({ min: 6 })
        .withMessage("minimal 6 karakter"),
    ],

    validator,
    userController.insert
);

module.exports = router;